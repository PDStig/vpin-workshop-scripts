# ***VPin Workshop JP DMD font image generator - 1.0.0 BETA***
# This Python script aims to make it easy to generate images for each character of the font you wish to use in JPSalas DMD.
#
# 1) Define all the options in CONFIG to your liking.
# 2) Install the PIL Python library.
# 3) Run this script.
#
# Your images will be generated in generated\d_char_{char_code}.png where char_code is the ASCII character code number.
#
# 4) Import the images into VPX via the image manager.
# 5) In the table script for JP DMD, replace where the Chars array is defined with this:
#       For i = 0 to 255 : Chars(i) = "d_empty" : Next
#       For i = 32 to 126 : Chars(i) = "d_char_" & i : Next 'ASCII
# 6) You may have to modify the original JP DMD script not to use its original FormatScore which relies on separate images containing numbers with decimals in them on the same image.
#
# CAVEATS:
# * This script has no concept of base lines. Thus, characters that normally hang below the base line (like lowercase g) will appear higher than normal in the DMD.
# * Some characters meant to appear small may appear larger than expected, such as periods and commas.
# * This script does not support combining punctuation with characters in the same image (which the original JP DMD does by default); all punctuation is treated as a separate character.
#
# This script was inspired from chatGPT.

from PIL import Image, ImageDraw, ImageFont

# CONFIG: Define the font properties
font_path = 'Squares Bold Free.otf'
image_size = (32, 32)               #(x,y) pixels
font_color = (255, 228, 177, 255)   #(red, green, blue, alpha)

# Create a blank image with transparent background
image = Image.new('RGBA', image_size, (0, 0, 0, 0))  # RGBA values for transparent

# Create a draw object
draw = ImageDraw.Draw(image)

# First, determine what font size we can use by looping through all the ASCII characters
font_size = image_size[1]
for char_code in range(32, 127):
    char = chr(char_code)  # Convert ASCII code to character

    # Load the font and calculate the width and height of the character at the initial font size
    font = ImageFont.truetype(font_path, font_size)
    char_width, char_height = draw.textsize(char, font=font)

    # Reduce the font size until the character fits within the image width
    while char_width >= image_size[0] or char_height >= image_size[1]:
        font_size -= 1
        font = ImageFont.truetype(font_path, font_size)
        char_width, char_height = draw.textsize(char, font=font)

# Loop through ASCII characters again, this time generating the actual images
for char_code in range(32, 127):
    char = chr(char_code)  # Convert ASCII code to character

    # Initialize the font size
    font_size = image_size[1]  # Start with the height of the image

    # Load the font and calculate the width and height of the character at the initial font size
    font = ImageFont.truetype(font_path, font_size)
    char_width, char_height = draw.textsize(char, font=font)

    # Calculate the position to center the character
    x = (image_size[0] - char_width) // 2
    y = (image_size[1] - char_height)

    # Draw the character on the image without antialiasing (to cut down on DMD fuzzy edges)
    draw.text((x, y), char, fill=font_color, font=font, antialias=False)

    # Save the image as PNG with transparency
    image.save(f'generated\\d_char_{char_code}.png')

    # Clear the image for the next character
    draw.rectangle([(0, 0), image_size], fill=(0, 0, 0, 0))

# Close the image
image.close()