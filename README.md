# VPin Workshop Scripts

Here is a repository of useful VBS scripts / libraries others can use in their Visual Pinball X tables. Please see the relevant script for more information on what it does.

## Version Tags
 - Alpha: The script did not have all the planned features implemented. Bugs are highly likely.
 - Beta: While all planned features were implemented, the script had not been thoroughly tested. Bugs are likely.
 - (No tag): The script is considered stable and bugs are not likely.

## Installation
Simply copy / paste the VBS code into your table script, or save the VBS file in your Scripts directory and execute it globally via this method:
```
    On Error Resume Next
    ExecuteGlobal GetTextFile("nameOfFile.vbs")
    If Err Then MsgBox "You need the nameOfFile.vbs file in your Scripts folder in order To run this table."
    On Error GoTo 0
```

## Usage
Each script has its own usage instructions. Please carefully read the comments in the script you desire to use.

## Authors and acknowledgment
The following people have contributed to one or more of these scripts:
 - Arelyel Krele (Patrick Schmalstig): Original author of all the scripts

## License
These scripts are licensed under the same license as [Visual Pinball X](https://github.com/vpinball/vpinball/blob/master/LICENSE).

## Project status
ACTIVE (but low priority)

## Issues
Please contact me at admin@pdstig.me if you have any issues with any scripts.
