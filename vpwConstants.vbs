'***************************************************************
' ZCON: VPIN WORKSHOP CONSTANTS - 1.0.0 BETA
'***************************************************************
' WHAT IS IT?
' The VPin Workshop Constants class is a quick / easy class that
' uses Scripting.Dictionary to define "constants" with types you
' cannot normally use on a Visual Basic constant (Const)
' (such as arrays or objects).
'
' HOW TO USE
' 1) Put this VBS file in your VPX Scripts folder, or
'    copy / paste the code into your table script
'    (and skip step 2).
' 2) Include this VBS file in your table via ExecuteGlobal and
'    Scripting.FileSystemObject.
' 3) Initialize the class on a variable:
'    Dim Scoring : set Scoring = new vpwConstants
' 4) Set values as you normally would with Scripting.Dictionary:
'    Scoring.Item("Bumpers") = 5000
'    -OR-
'    Scoring.Add "Bumpers", 5000
' 5) Once a key has a non-empty value, it cannot be set again.
'
' TUTORIAL: https://youtu.be/w1ZhxoNogWE
'***************************************************************

Class vpwConstants
	' Create the private dictionary
	Private mDict
	Private Sub Class_Initialize
		Set mDict = CreateObject("Scripting.Dictionary")
	End Sub
	
	' Get the count of the number of constants / keys
	Public Property Get Count
		Count = mDict.Count
	End Property
	
	' Get the value of a constant key
	Public Property Get Item(aKey)
		Item = Empty
		If mDict.Exists(aKey) Then
			If IsObject(mDict(aKey)) Then
				Set Item = mDict(aKey)
			Else
				Item = mDict(aKey)
			End If
		End If
	End Property
	
	' Set a constant value
	Public Property Let Item(aKey, aData)
		setConstant aKey, aData
	End Property
	
	Public Property Set Key(aKey)
		' This function is (and always has been) a no-op. Previous definition
		' just looked up aKey in the keys list, and if found, set the key to itself.
	End Property
	
	' Add a constant key (same as setting its value)
	Public Sub Add(aKey, aData)
		setConstant aKey, aData
	End Sub
	
	' The actualizer for setting a constant
	Private Sub setConstant(aKey, aData)
		' If the constant key does not already exist, create / set it. Otherwise, do nothing (disallow overwriting / changing constants!)
		If Not mDict.Exists(aKey) Then
			If IsObject(aData) Then
				Set mDict(aKey) = aData
			Else
				mDict(aKey) = aData
			End If
		End If
	End Sub
	
	' No remove functions because we want to disallow removing constants!
	
	Public Function Exists(aKey)
		Exists = mDict.Exists(aKey)
	End Function
	Public Function Items
		Items = mDict.Items
	End Function
	Public Function Keys
		Keys = mDict.Keys
	End Function
End Class

'**************************************************************
'   END VPWCONSTANTS
'**************************************************************